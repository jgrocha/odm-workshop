# Making Point Clouds with OpenDroneMap

## QGIS ❤ Point clouds

## Create your own point clouds

In GIS, we are mostly interested in point clouds from the outdoor, covering small to large areas.

Two common ways to get potin clouds:
- Using LIDAR
- Using images (and SFM algoritms)

### LIDAR

LIDAR is usually more precise, but more expensive. Hard to DIY. 

The major usage is on the top of vehicles, scanning at "ground" level. It can be used in a backpack, for off road scanning.

To use it from the air, it requires a profissional drone (for small areas). The cost of the sensor + the cost of the drone makes this a quite expensive solution.

### Images

Images are less expensive. Cameras are becaming better every day. Images are easy to capture, but requires more post-processing to get a point cloud.

Vídeo can be used too. But usually the fames have less definition than the pictures. In our workflow, we will use just images.

## Image acquisition

You need:
- a drone with camera and GPS
- flight planning and image acquisition app
- a high precision GPS to survey ground control points

#### Drones

There are so many. I'm flying with a [DJI Mavic 2 PRO](https://www.dji.com/pt/mavic-2), with an Hasselblad camera.

#### Flight planning software

I'm using [Pix4DCapture](https://www.pix4d.com/product/pix4dcapture). It is a free drone flight planning app. It includes several types of flight plans and it is able to upload the plan to several drone brands.

Which flight plan should we use? It depends on what we want from the images. 

Types of flight plan



Some introductory good pratices:

- Estimate the flight height (altitude) according to your target resolution (it might not be so simple to compute). If you fly above, you don't get it; if you fly below, you will have much more images, more time consuming processing and your result will not look better.
- If you are insterested in maps (you want to have a ortophoto and DSM/DTM), do a grid flight with the camare at 90º (nadir).
- If you are interested in point clouds, do a double grid flight with camera at a certain angle (like 70º).
- If you interested in maps and point clouds... do two flights: one using nadir, at a higher altitude, like 70m, and another flight

![Image by Piero Toffanin](https://md.geomaster.pt/uploads/upload_bdca76a646270250f4cd33ee50310d1e.png)

## Creating Point Clouds with OpenDroneMap

OpenDroneMap is a large collecting of tools. According to our processing options, OpenDroneMap will use the adequate pipeline to get the job done. 

We started talking about point clouds, but OpenDroneMap is able to produce much more than point clouds, as we will see in the examples. It produces rectified ortophotos, DSM, DTM, etc.

To make it even simpler, we will use WebODM. WebODM is OpenDrone Map encapsulated behind a web interface. The installation is based on docker images. It will take a few minutes to run, for the first time, but aftrwards, it will run immediatly.

Since it is using docker, the installation is quite simple. Sometimes it not so simple in Windows, but this is related to the virtualization support in Windows. 

### Installation

#### Debian/Ubuntu

#### Windows

### First run


### Sample data

Project 4 - Paradela do Vouga

Project 6 - Uminho com luz partida: bom para mostrar as horas do dia

### Second run

#### Ground control points

**CRS**

OpenDroneMap have to do heavy maths on the background. It always uses a rectangular coordinate system. You can write down a file with your GCP in any CRS, but OpenDroneMap will convert them to the nearest WGS84 UTM projection. Since you are a GIS expert, it is better to convert the coordinates by yourself in advance.

You know in which UTM zone you are, but for those you don't, there are online tools that can help, like [mangomap](https://mangomap.com/robertyoung/maps/69585/what-utm-zone-am-i-in-#).

**Number of GCP**

From 5 to 8, placed evenly. If GCP exist, ODM will use them to reference the data (not the coordinates x«extracted from the drone images). It will be more accurate.

**Capturing GCP markers***

To do a proper triangulation, the marker needs to be captures by, at least, 3 images. Make sure that you flight covers properly the GCP markers on the ground. AFterwards, you will have to identify the location of each GCP markers in 3 images, at least.

**GCP markers**

Tiles are quite good! Use mate tiles, of two different colors. Any hand made GCP markers are good. Mine are cutting boards painted on one side.

![](https://md.geomaster.pt/uploads/upload_d0bc0d3f927963a6a2e6a7711e0c5834.jpg)

#### GCP Surveying

Current GPS devices included in our smartphones are limited in precision. Mostly because they use just one GPS frequency.

**Low cost precise GPS devices**

Right now, GPS chip producers are making this chips more powerful (more accurate clocks, more frequencies/GNSS covered, more processing capabilities, etc) an less expensive.

Hardware (external GPS)

[Ardusimple](https://www.ardusimple.com/) are using uBlox last generation chips to assemble low cost solutions with centimeter precision. Their [handheld surveyor kit](https://www.ardusimple.com/product/rtk-handheld-surveyor-kit/) cost 400,00 € and you will have everuthing you need.

To have centimeter precision accuracy with just one receiver, you need to use your national/local NTRIP provider. I use the national NTRIP network, provided by the national mapping agency. The service is free. You just need to request a login and password.

If you don't have realtime corrections, you can do post processing corrections (using RTKLIB) or buy a base + rover solution, available from 550,00 €.

Software (on Android)

I use [SW Maps](https://play.google.com/store/apps/details?id=np.com.softwel.swmaps), made by Avinab Malla, from Nepal. It has all we need: connect to an external GPS, connection to a NTRIP caster service, interface to collect field data. I usually collect the GCP location and the associated images. Point locations show be collected by keeping the GPS in the same place for *some* time. If you have a clear sky view and the device reports a high precision, just wait one or two minutes. Always confirm that you have a RTK Fix when sampling the position.

Geoopackage → Concert to UTM → export as text file

Compute the $x, $y and z($geometry)

![](https://md.geomaster.pt/uploads/upload_ae6adbfb0f38494ebb25819c26e06a0c.png)

Export to ODS

Make sure you follow this structure:
```
X,Y,Z,px,py,Label
```

![](https://md.geomaster.pt/uploads/upload_e8099c867142f65feb219f6f944166b9.png)


Export to CSV

Save As...  using the extension .txt (not .csv)

Then add the header (the CRS proj string):
```
+proj=utm +zone=29 +datum=WGS84 +units=m +no_defs
```

The initial GCP file should be like this:
```
+proj=utm +zone=29 +datum=WGS84 +units=m +no_defs
549826.821193825	4599616.48139274	234.984954545455	0	0	Gcp_1
549782.228041844	4599684.55577769	234.776166666667	0	0	Gcp_3
549834.102593827	4599759.21749988	239.784912280702	0	0	Gcp_4
549966.4363039	4599727.56110875	238.898384615385	0	0	Gcp_5
549949.415488829	4599575.44720724	237.430344262295	0	0	Gcp_8
```

![](https://md.geomaster.pt/uploads/upload_069ccb35f4592ba18d380a0c5dda9e45.png)





## Other tools to explore

- PDAL is Point Data Abstraction Library (its the *GDAL for Point Clouds*). It allows you to compose operations on point clouds into pipelines of stages.
- Potree is a free open-source WebGL based point cloud renderer for large point clouds.
- PostgreSQL pointcloud extension https://github.com/pgpointcloud/pointcloud
- Entwine is a data organization library for massive point clouds.

### PDAL examples

- Reprojecting a point cloud 
- Clipping a point cloud https://pdal.io/workshop/exercises/analysis/clipping/clipping.html